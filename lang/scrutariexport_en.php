<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'affecter_corpus' => 'Include into a corpus',
	'auteur_spip' => 'Use Spip authors as a supplementary field',
	'authority_uuid' => 'Authority name',

	'base_name' => 'Base Name',

	'confirmer_supprimer_corpus' => 'Are you sure to delete this corpus ?',
	'confirmer_supprimer_export' => 'Are you sure to delete this export ?',
	'confirmer_supprimer_thesaurus' => 'Are you sure to delete this thesaurus ?',
	'corpus_de_l_export' => 'Corpus included in this export',
	'creer_corpus' => 'Create a new corpus',
	'creer_export' => 'Create a new export',
	'creer_thesaurus' => 'Create a new thesaurus',

	'definition_export_scrutari' => 'Scrutari Export configuration',

	'exclure_de_l_export' => 'Exclude from export: ',

	'format_non_conforme' => 'Wrong format',

	'gerer_corpus' => 'Manage corpus',
	'gerer_exports' => 'Manage export',
	'gerer_thesaurus' => 'Manage thesaurus',
	'groupes_thesaurus' => 'keyword groups included in the thesaurus',
	'groupes_du_thesaurus' => 'Keyword groups included in the thesaurus',

	'icone_menu_config' => 'Scrutari Export',
	'icone_supprimer_corpus' => 'Delete this corpus',
	'icone_supprimer_export' => 'Delete this export',
	'icone_supprimer_thesaurus' => 'Delete this thesaurus',
	'info_aucun_corpus' => 'Section not included in a corpus',
	'info_page_corpus' => 'Corpus management page',
	'info_page_exports' => 'Export management page',
	'info_page_thesaurus' => 'Thesaurus management page',
	'info_retirer_corpus' => 'Remove from corpus',
	'intitule_champ_auteur' => 'Label of author field',
	'intitule_corpus' => 'Corpus short title',
	'intitule_long' => 'Corpus long title',
	'intitule_thesaurus' => 'Thesaurus title',
	'intitule_fiche' => 'Article label',
	'intitule_short' => 'Short title',

	'liste_corpus' => 'Corpus list',
	'liste_exports' => 'Export list',
	'liste_thesaurus' => 'Thesaurus list',

	'modifier_corpus' => 'Edit this corpus',
	'modifier_export' => 'Edit this export',
	'modifier_thesaurus' => 'Edit this thésaurus',


	'nom_champ_auteur' => 'author',
	'nom_deja_existant' => 'Existing name',
	'nom_technique' => 'Technical name',

	'rubriques_corpus' => 'sections included in the corpus',
	'rubriques_du_corpus' => 'Sections included in the corpus',

	'statut_scrutari' => 'Scrutari state of this article',
	'selectionner_un_corpus' => 'Select a corpus',

	'thesaurus_de_l_export' => 'Export thesaurus',
	'titre_scrutariexport' => 'Scrutari Export',

	'' => ''
);

?>
