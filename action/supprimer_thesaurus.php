<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

function action_supprimer_thesaurus_dist(){
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	if ($id_thesaurus = intval($arg)
		AND autoriser('administrer', 'scrutariexport')
	){
		include_spip('action/editer_thesaurus');
		thesaurus_supprimer($id_thesaurus);
	}
}

?>
