<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * affecter le statut scrutari a un article
 *
 * @return array
 */
function action_affecter_statut_scrutari_dist($id_article = null){
	if (is_null($id_article)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_article = $securiser_action();
	}

	/* on réinitialise avant d'affecter */
	sql_delete('spip_exportscrutari_articles_exclus', 'id_article=' . intval($id_article));

	if (_request('exports')){

		foreach (_request('exports') as $id_exportscrutari){

			sql_insertq('spip_exportscrutari_articles_exclus', array('id_article' => $id_article, 'id_exportscrutari' => $id_exportscrutari));

		}

	}

	return array($id_article);
}

?>
