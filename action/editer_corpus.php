<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * editer un corpus (action apres creation/modif de corpus)
 *
 * @return array
 */
function action_editer_corpus_dist($arg = null){

	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	if (!$id_corpus = intval($arg)){
		$id_corpus = corpus_inserer();
	}

	$err = corpus_modifier($id_corpus);

	return array($id_corpus, $err);
}

/**
 * Creer un nouveau corpus
 *
 * @return int
 */
function corpus_inserer(){

	include_spip('inc/autoriser');
	if (!autoriser('administrer', 'scrutariexport'))
		return false;

	$champs = array();
	// Envoyer aux plugins
	$champs = pipeline('pre_insertion',
		array(
			'args' => array(
				'table' => 'spip_corpus',
			),
			'data' => $champs
		)
	);
	$id_corpus = sql_insertq("spip_corpus", $champs);
	pipeline('post_insertion',
		array(
			'args' => array(
				'table' => 'spip_corpus',
				'id_objet' => $id_corpus
			),
			'data' => $champs
		)
	);
	return $id_corpus;
}

/**
 * Modifier un corpus
 *
 * @param int $id_corpus
 * @param array $set
 * @return string|bool
 */
function corpus_modifier($id_corpus, $set = null){

	include_spip('inc/modifier');
	$c = collecter_requests(
	// white list
		array('name', 'intitule_corpus', 'intitule_fiche', 'auteur_spip', 'intitule_champ_auteur'),
		// black list
		array(),
		// donnees eventuellement fournies
		$set
	);

	if ($err = objet_modifier_champs('corpus', $id_corpus,
		array(),
		$c)
	)
		return $err;

	corpus_lier($id_corpus, 'rubrique', _request('rubriques'), 'set');
	return $err;

}

/**
 * Mettre a jour les liens objets/corpus.
 *
 * @param int|array $corpus
 * @param string $type
 * @param int|array $ids
 * @param string $operation
 */
function corpus_lier($corpus, $type, $ids, $operation = 'add'){
	include_spip('inc/autoriser');
	include_spip('action/editer_liens');
	if (!$corpus)
		$corpus = "*";
	if (!$ids)
		$ids = array();
	elseif (!is_array($ids))
		$ids = array($ids);

	if ($operation=='del'){
		// on supprime les ids listes
		objet_dissocier(array('corpus' => $corpus), array($type => $ids));
	} else {
		// si c'est une affectation exhaustive, supprimer les existants qui ne sont pas dans ids
		// si c'est un ajout, ne rien effacer
		if ($operation=='set'){
			objet_dissocier(array('corpus' => $corpus), array($type => array("NOT", $ids)));
			// bugfix temporaire : objet_associer ne gere pas id=0
			if (!in_array(0, $ids) AND $type=="rubrique")
				sql_delete("spip_corpus_liens", "id_corpus=" . intval($corpus) . " AND id_objet=0 AND objet=" . sql_quote($type));
		}
		foreach ($ids as $id){
			if (autoriser('administrer', 'scrutariexport')){
				// bugfix temporaire : objet_associer ne gere pas id=0
				if ($id==0 AND $type=="rubrique")
					sql_insertq("spip_corpus_liens", array('id_corpus' => $corpus, "id_objet" => $id, "objet" => $type));
				else
					objet_associer(array('corpus' => $corpus), array($type => $id));
			}
		}
	}
}

/**
 * Supprimer un corpus
 *
 * @param int $id_corpus
 * @return int
 */
function corpus_supprimer($id_corpus){
	include_spip('action/editer_liens');
	objet_dissocier(array('corpus' => $id_corpus), array('*' => '*'));

	sql_delete("spip_corpus", "id_corpus=" . intval($id_corpus));

	$id_corpus = 0;
	return $id_corpus;
}

?>
