<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

function formulaires_affecter_statut_scrutari_charger_dist($id_article){

	include_spip('base/abstract_sql');
	/* Tester si l'article est dans un corpus */
	$corpus = sql_countsel('spip_articles,spip_corpus_liens', 'spip_articles.id_rubrique=spip_corpus_liens.id_objet AND spip_corpus_liens.objet="rubrique" AND spip_articles.id_article=' . intval($id_article));

	if (!$corpus){
		$valeurs['editable'] = false;
	} else {
		$exports = sql_allfetsel('id_exportscrutari', 'spip_exportscrutari_articles_exclus', 'id_article=' . intval($id_article));

		foreach ($exports as $export){
			$valeurs['exports'][] = $export['id_exportscrutari'];
		}

		$valeurs['id_article'] = $id_article;
	}

	return $valeurs;
}

function formulaires_affecter_statut_scrutari_traiter_dist($id_article){
	//include_spip('action/editer_scrutari_statut');
	//scrutariexport_revision_corpus_objets_lies(intval(_request('corpus')),$id_rubrique,'rubrique');

	$affecter_statut_scrutari = charger_fonction('affecter_statut_scrutari', 'action');
	$arg = $affecter_statut_scrutari($id_article);

	return array('editable' => true, 'message' => '');
}
