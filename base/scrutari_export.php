<?php
/**
 * Plugin Scrutari Export
 * Licence GPL (c) 2011 Coredem
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

function scrutari_export_declarer_tables_interfaces($interface){

	//-- Table des tables ----------------------------------------------------

	$interface['table_des_tables']['corpus'] = 'corpus';
	$interface['table_des_tables']['thesaurus'] = 'thesaurus';
	$interface['table_des_tables']['exportscrutari'] = 'exportscrutari';
	$interface['table_des_tables']['exportscrutari_articles_exclus'] = 'exportscrutari_articles_exclus';

	return $interface;
}

function scrutari_export_declarer_tables_objets_sql($tables){

	$tables['spip_exportscrutari'] = array(
		'type' => 'exportscrutari',
		'principale' => 'oui',
		'field' => array(
			"id_exportscrutari" => "bigint(21) NOT NULL",
			"authority_uuid" => "varchar(255) DEFAULT '' NOT NULL",
			"base_name" => "varchar(255) DEFAULT '' NOT NULL",
			"intitule_short" => "text DEFAULT '' NOT NULL",
			"intitule_long" => "text DEFAULT '' NOT NULL",
			"base_icon" => "text DEFAULT '' NOT NULL"
		),
		'key' => array(
			'PRIMARY KEY' => 'id_exportscrutari'
		),
		'titre' => "intitule_long AS titre, '' AS lang",
		'tables_jointures' => array(
			'exportscrutari_liens'
		)
	);

	$tables['spip_corpus'] = array(
		'type' => 'corpus',
		'texte_modifier' => 'scrutariexport:modifier_corpus',
		'principale' => 'oui',
		'field' => array(
			"id_corpus" => "bigint(21) NOT NULL",
			"name" => "varchar(255) DEFAULT '' NOT NULL",
			"intitule_corpus" => "text DEFAULT '' NOT NULL",
			"intitule_fiche" => "text DEFAULT '' NOT NULL",
			"auteur_spip" => "ENUM('non', 'oui') DEFAULT 'non' NOT NULL",
			"intitule_champ_auteur" => "text DEFAULT ''",
			"maj" => "TIMESTAMP"
		),
		'key' => array(
			'PRIMARY KEY' => 'id_corpus',
		),
		'titre' => "intitule_corpus AS titre, '' AS lang",
		'tables_jointures' => array(
			'corpus_liens',
			'exportscrutari_liens'
		)
	);

	$tables['spip_thesaurus'] = array(
		'type' => 'thesaurus',
		'texte_modifier' => 'scrutariexport:modifier_thesaurus',
		'principale' => 'oui',
		'field' => array(
			"id_thesaurus" => "bigint(21) NOT NULL",
			"name" => "varchar(255) DEFAULT '' NOT NULL",
			"intitule_thesaurus" => "text DEFAULT '' NOT NULL",
			"maj" => "TIMESTAMP"
		),
		'key' => array(
			'PRIMARY KEY' => 'id_thesaurus'
		),
		'titre' => "intitule_thesaurus AS titre, '' AS lang",
		'tables_jointures' => array(
			'thesaurus_liens',
			'exportscrutari_liens'
		)
	);

	$tables['spip_rubriques']['tables_jointures'][] = 'corpus_liens';
	$tables['spip_groupes_mots']['tables_jointures'][] = 'thesaurus_liens';


	return $tables;
}

function scrutari_export_declarer_tables_auxiliaires($tables_auxiliaires){

	$spip_exportscrutari_liens = array(
		"id_exportscrutari" => "bigint(21) DEFAULT '0' NOT NULL",
		"id_objet" => "bigint(21) DEFAULT '0' NOT NULL",
		"objet" => "VARCHAR (25) DEFAULT '' NOT NULL"
	);

	$spip_exportscrutari_liens_key = array(
		"PRIMARY KEY" => "id_exportscrutari,id_objet,objet",
		"KEY id_zone" => "id_exportscrutari"
	);

	$tables_auxiliaires['spip_exportscrutari_liens'] = array(
		'field' => &$spip_exportscrutari_liens,
		'key' => &$spip_exportscrutari_liens_key
	);

	$spip_corpus_liens = array(
		"id_corpus" => "bigint(21) DEFAULT '0' NOT NULL",
		"id_objet" => "bigint(21) DEFAULT '0' NOT NULL",
		"objet" => "VARCHAR (25) DEFAULT '' NOT NULL"
	);

	$spip_corpus_liens_key = array(
		"PRIMARY KEY" => "id_corpus,id_objet,objet",
		"KEY id_zone" => "id_corpus"
	);

	$tables_auxiliaires['spip_corpus_liens'] = array(
		'field' => &$spip_corpus_liens,
		'key' => &$spip_corpus_liens_key
	);

	$spip_thesaurus_liens = array(
		"id_thesaurus" => "bigint(21) DEFAULT '0' NOT NULL",
		"id_objet" => "bigint(21) DEFAULT '0' NOT NULL",
		"objet" => "VARCHAR (25) DEFAULT '' NOT NULL"
	);

	$spip_thesaurus_liens_key = array(
		"PRIMARY KEY" => "id_thesaurus,id_objet,objet",
		"KEY id_zone" => "id_thesaurus"
	);

	$tables_auxiliaires['spip_thesaurus_liens'] = array(
		'field' => &$spip_thesaurus_liens,
		'key' => &$spip_thesaurus_liens_key
	);

	$spip_exportscrutari_articles_exclus = array(
		"id_article" => "bigint(21) DEFAULT '0' NOT NULL",
		"id_exportscrutari" => "bigint(21) DEFAULT '0' NOT NULL",
	);

	$spip_exportscrutari_articles_exclus_key = array(
		"PRIMARY KEY" => "id_article,id_exportscrutari",
		"KEY id_exportscrutari" => "id_exportscrutari"
	);

	$tables_auxiliaires['spip_exportscrutari_articles_exclus'] = array(
		'field' => &$spip_exportscrutari_articles_exclus,
		'key' => &$spip_exportscrutari_articles_exclus_key
	);

	return $tables_auxiliaires;
}

?>
